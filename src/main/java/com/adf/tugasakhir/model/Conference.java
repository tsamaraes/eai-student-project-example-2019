package com.adf.tugasakhir.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Conference model.
 */
@Entity
@Data
@Table(name = "conference")
public class Conference {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nama")
    private String nama;

    @NotNull
    @Column(name = "tanggal_mulai")
    private Date tanggalMulai;

    @NotNull
    @Column(name = "ruangan_dipakai")
    private String ruanganDipakai;

    @NotNull
    @Column(name = "abstrak")
    private String abstrak;
}