package com.adf.tugasakhir.controller;

import java.sql.Date;

import javax.persistence.EntityNotFoundException;

import com.adf.tugasakhir.dataclass.PaperResult;
import com.adf.tugasakhir.model.Conference;
import com.adf.tugasakhir.repository.ConferenceRepo;
import com.adf.tugasakhir.service.ConferenceService;
import com.adf.tugasakhir.service.PaperFinderService;

import io.micrometer.core.annotation.Timed;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * HomeController TODO add a Update method for conference. Just implement CRD.
 * TODO: show the abstract as a text area or something (not text capsule)
 */

@Log
@Controller
public class HomeController {

    @Autowired
    PaperFinderService paperFinderService;

    @Autowired
    ConferenceService conferenceService;

    @Autowired
    ConferenceRepo conferenceRepo; // testing purposes for testdb only. delete later.

    @Timed("home")
    @GetMapping("/")
    @PreAuthorize("isAuthenticated()")
    public String home(Model model) {
        log.info("Homepage");
        model.addAttribute("conferenceList", conferenceService.getAllLatestConference());
        return "_home";
    }

    @GetMapping("/add-conference")
    @PreAuthorize("isAuthenticated()")
    public String addConferenceForm(Model model) {
        log.info("Displaying add conference form");
        model.addAttribute("conference", new Conference());
        return "add-conference";
    }

    @PostMapping("/add-conference")
    @PreAuthorize("isAuthenticated()")
    public String addConferenceSubmit(@ModelAttribute Conference newConference, BindingResult bindingResult,
            RedirectAttributes redAttr) {
        if (bindingResult.hasErrors()) {
            log.info("Value have errors");
            redAttr.addFlashAttribute("error_msg", "Terdapat kesalahan pada form ada.");
            return "redirect:/goof";
        }
        conferenceRepo.save(newConference);
        log.info("Add conference");
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    @PreAuthorize("isAuthenticated()")
    public String updateConferenceForm(@PathVariable("id") long id, RedirectAttributes redirectAttributes,
            Model model) {

        try {
            model.addAttribute("conference", conferenceService.getConferenceById(id));
        } catch (EntityNotFoundException e) {
            log.info("Conference doesn't exist");
            redirectAttributes.addFlashAttribute("error_msg", "Conference not found.");
            return "redirect:/goof";
        }
        log.info("Conference updated");
        return "update-conference";
    }

    @PostMapping("/update-conference")
    @PreAuthorize("isAuthenticated()")
    public String updateConferenceSubmit(@ModelAttribute Conference conference, RedirectAttributes redirectAttributes) {
        log.info("Update conference");
        try {
            conferenceService.updateConference(conference);
        } catch (EntityNotFoundException e) {
            redirectAttributes.addFlashAttribute("error_msg", "The conference that you want to update doesn't exist.");
            return "redirect:/goof";
        }
        return "redirect:/";
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String deleteConference(@PathVariable("id") long id) {
        conferenceService.removeConferenceById(id);
        log.info("Conference deleted");
        return "redirect:/";
    }

    @Timed("extractKeyPhrases")
    @GetMapping("/view/{id}")
    @PreAuthorize("isAuthenticated()")
    public String viewConference(@PathVariable("id") long id, Model model, RedirectAttributes redAttr) {
        try {
            Conference conference = conferenceService.getConferenceById(id);
            model.addAttribute("conference", conference);
            PaperResult pResult = paperFinderService.findPaperFromConference(conference);

            if (!pResult.isError()) {
                model.addAttribute("paperList", pResult.getPaperList());
            }
            log.info("View conference");
            return "conference-detail";
        } catch (EntityNotFoundException e) {
            log.info("Conference not found");
            redAttr.addFlashAttribute("error_msg", "Conference not found.");
            return "redirect:/goof";
        }
    }

    @GetMapping("/testdb")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public String home() {
        Conference testConference = new Conference();
        testConference.setNama("Cloud");
        testConference.setAbstrak("Computing");
        testConference.setRuanganDipakai("123");
        testConference.setTanggalMulai(new Date(0));

        if (conferenceRepo.findByNama("Cloud").size() > 0) {
            return "Yep, there's already one in the database. It's working.";
        } else {
            conferenceRepo.save(testConference);
        }

        return "If you're seeing this, then most likely the test is finished smoothly";
    }

    @Timed("goof")
    @GetMapping("/goof")
    @PreAuthorize("isAuthenticated()")
    public String customErrorPage(Model model) {
        return "error/custom_error";
    }
}