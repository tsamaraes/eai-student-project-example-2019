package com.adf.tugasakhir.service;

import com.adf.tugasakhir.dataclass.PaperResult;
import com.adf.tugasakhir.model.Conference;

/**
 * PaperFinder A service in which it will give you every solution regarding
 * searching for a paper.
 */
public interface PaperFinderService {

    /**
     * Will give a list of paper (if found) about the current conference.
     * 
     * @param conference
     * @return
     */
    PaperResult findPaperFromConference(Conference conference);
}
