package com.adf.tugasakhir.service;

import com.adf.tugasakhir.client.RestClientSingleton;
import com.adf.tugasakhir.dataclass.Documents;
import com.adf.tugasakhir.dataclass.KeyPhrasesResult;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * TextAnalysisServiceImpl
 * This class will implement the TextAnalysisService
 * Also acts as a client for the text analysis on microsoft for convenience.
 */
@Service
public class TextAnalysisServiceImpl implements TextAnalysisService {
    
    public static final String KEY_PHRASES_ANALYSIS_ENDPOINT = System.getenv("KEY_PHRASES_ANALYSIS_ENDPOINT");

    private String TEXT_ANALYTICS_API_KEY = System.getenv("TEXT_ANALYTICS_API_KEY");

    @Override
    public KeyPhrasesResult extractKeyPhrases(Documents documents) {
        RestTemplate restTemplate = RestClientSingleton.getTemplate();
        HttpEntity<Documents> request = new HttpEntity<>(documents, createHeaders());
        return restTemplate.postForObject(KEY_PHRASES_ANALYSIS_ENDPOINT, request, KeyPhrasesResult.class);
    }


    /**
     * Will give you the custom headers required to use the microsoft service
     * Adapted from this https://stackoverflow.com/questions/21723183/spring-resttemplate-to-post-request-with-custom-headers-and-a-request-object
     * @return
     */
    private MultiValueMap<String, String> createHeaders() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

        // This is not the best way to configure a secret key.
        headers.add("Ocp-Apim-Subscription-Key", TEXT_ANALYTICS_API_KEY);
        headers.add("Content-Type", "application/json");
        return headers;
    }
    
}