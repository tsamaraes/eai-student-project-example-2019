# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> Name: Tsamara Esperanti Erwin
> NPM: 1806173563

### Task 1: Twelve Factor App
1. The value of text.analytics.api.key is committed on the application.properties file. 
This violates the 12 Factor App (Config) because the value is supposed to be stored in the environment variable.
I store the value in an environment variable called TEXT_ANALYTICS_API_KEY. We can get the value from the environment variable.
2. In the TextAnalysisServiceImpl class, there's a string called KEY_PHRASES_ANALYSIS_ENDPOINT 
that contains link to a resource. These external credentials should be stored in the environment variable as well. 

> Code after using environment variables:
> - ![appproperties](images/image2.png)
> - ![TextServiceImplWithEnvVar](images/image1.png)

3. I added logging on each controller methods.

### Task 2
> ![prometheus](images/image3.png)

The graph represents how many times viewConference is invoked.